<?php
// register nav walker class alias
require_once('wp_bootstrap_navwalker.php');


// theme support
function wpowo_theme_setup(){
	add_theme_support( 'post-thumbnails' );
// add theme support for post formats

	add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );

	register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'THEMENAME' ),
) );
}
add_action('after_setup_theme','wpowo_theme_setup');

// write a function to limit the length of excerpts shown
function wpowo_set_excerpt_length(){
        return 20;
}
add_filter( 'excerpt_length','wpowo_set_excerpt_length' );


/**
 * Register our sidebars and widgetized areas.
 *
 */
function wpowo_right_widgets_init() {

	register_sidebar( array(
		'name'          => 'Page Right Side Bar',
		'id'            => 'page_right_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="page-sidebar">',
		'after_title'   => '</h2>',
	) );


	// register sidebar widget areas for box1
	register_sidebar( array(
		'name'          => 'Front Page Box 1',
		'id'            => 'box1',
		'before_widget' => '<div class="box">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	// register sidebar widget areas for box2
	register_sidebar( array(
		'name'          => 'Front Page Box 2',
		'id'            => 'box2',
		'before_widget' => '<div class="box">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	// register sidebar widget areas for box3
	register_sidebar( array(
		'name'          => 'Front Page Box 3',
		'id'            => 'box3',
		'before_widget' => '<div class="box">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

}
add_action( 'widgets_init', 'wpowo_right_widgets_init' );



function wpowo_footer() {
    echo "<footer class='blog-footer'>,
     <p>&copy; ";
    echo" <?php Date('Y') -  bloginfo('name') ?>";
    echo " </p><p> <a href="#">Back to top</a> </p></footer>"
     ;
}
add_action( 'wp_footer', 'wpowo_footer' );
