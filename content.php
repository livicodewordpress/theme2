
                  <div class="blog-post">
                      <h2 class="blog-post-title">

                      <!-- if it's a single post do not show the link -->
                      <?php 
                      		if(is_single(  )) : ?>
                      			<?php the_title(); ?>
                      	<?php else : ?>
                      		<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                      	
                      <?php endif; ?>
                      
                      </h2>
                      <div class="blog-post-meta"><?php the_time('F j, Y'); ?> by <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"> <?php the_author(); ?></a></p>

                      <p><?php if ( has_post_thumbnail() ) { ?>
                          <div class="post-thumb"><?php the_post_thumbnail(); ?>
                          </div>
                        <?php } ?>
                        <!-- if it's single post I want the content to display -->
                       <?php if(is_single(  )) : ?>
                       		<?php the_content(); ?>
                       <?php else :?>
                       		 <?php the_excerpt(); ?>
                       <?php endif;?>

                       <?php if(is_single(  )) :?>
                       		<?php comments_template();?>
                       
                       	<?php endif;?>

                       
                          
                       </div>
                      <hr>
                   </div>