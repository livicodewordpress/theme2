<?php get_header();?>

      <div class="row">

        <div class="col-sm-8 blog-main">
          <?php
              if ( have_posts() ) :
                while ( have_posts() ) : the_post();  ?>
              
                  <?php get_template_part('content',get_post_format());?>
              <?php  endwhile; ?>
            <?php else :
                echo wpautop( 'Sorry, no posts were found' );
              endif;
          ?>

          
          <nav>
            <?php posts_nav_link(); ?>
          </nav>

        </div><!-- /.blog-main -->


      </div><!-- /.row -->

    </div><!-- /.container -->

 <?php get_footer();?>