<?php get_header();?>

      <div class="row">

        <div class="col-sm-8 blog-main">
          <?php
              if ( have_posts() ) :
                while ( have_posts() ) : the_post();  ?>
              
                  <div class="blog-post">
                      <h2 class="blog-post-title">
                     <?php the_title(); ?>
                      </h2>
                         <?php if ( has_post_thumbnail() ) { ?>
                          <div class="post-thumb"><?php the_post_thumbnail(); ?>
                          </div>
                        <?php } ?>                  
                        <?php the_content(); ?>
                          
                       </p>
                      <hr>
                   </div>
              <?php  endwhile; ?>
            <?php else :
                echo wpautop( 'Sorry, no posts were found' );
              endif;
          ?>

          
          <nav>
            <?php posts_nav_link(); ?>
          </nav>

        </div><!-- /.blog-main -->



 <?php get_footer();?>